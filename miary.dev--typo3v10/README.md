# typo3v10 ansible

Ping

```
cd ansible/
source bin/activate
ansible -i commun/inventaire.ini -m ping kimsufi --ask-pass
or
ansible -i /var/www/ansible_tasks/miary.dev--typo3v10/commun/inventaire.ini -m ping kimsufi --ask-pass
```

Run playbook

Create file commun/inventaire.ini inspiring from commun/inventaire.ini-dist


```
ansible-playbook -i /var/www/ansible_tasks/miary.dev--typo3v10/commun/inventaire.ini --become --ask-become-pass --ask-vault-pass  /var/www/ansible_tasks/miary.dev--typo3v10/install.yml
```
